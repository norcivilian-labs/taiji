import { h } from "preact";
import Router, { route} from "preact-router";
import styles from './app.module.css';
import { Detection, Login, Menu, Profile, Signup } from './layout/index.js'
import { useState, useEffect } from 'preact/hooks'


export default function App() {
  const [token, setToken] = useState('');

  //  проверяем наличие токена в кэше
  const checkToken = () => {
    const token = sessionStorage.getItem("token");
    const hasNotToken = token === null;
    if (hasNotToken) {
      route('/login', true);
    } else {
      setToken(token);
    }
  };

    // Запрос данных при открытии экрана профиля
    useEffect(() => {
      checkToken();
    }, []);
    
      // После успешного входа сохраняем токен в кэше
      const handleLogin = async (token) => {
        try {
          sessionStorage.setItem("token", token);
          setToken(token);
          console.log('Токен успешно сохранен в кэше');
        } catch (error) {
          console.error('Ошибка входа:', error);
        }
      }


  return (
    <div>
      <Router>
        <Login path="/login"  handleLogin={handleLogin} />
        <Menu path="/" token={token} />
        <Detection path="/fundus" token={token} />
        <Profile path="/profile" token={token} />
        <Detection path="/okt" token={token} />
        <Signup path="/signup" />
      </Router>
    </div>
  );
}
