export class BrowserAPI {
  constructor() {}

  async login(email, password) {
    if (email, password === undefined) {
      console.error('login, password undefined');
    }

    try {
      const response = await fetch(
        'https://functions.yandexcloud.net/d4esr6ie5khkuno72hib',
        {
          method: "post",
          headers: { 'Accept': 'application/json', 'Content-Type': 'text/plain' },
          body: JSON.stringify({
            "op": "login",
            email,
            password,
          })
        }
      );

      if (response.status === 200) {
        // Добавить обработку токена или переход к другому экрану после успешного входа
        const { token } = await response.json()

        // TODO: validate token

        return token
      } else {
        console.error('Login failed');
        // Добавить обработку ошибок, например, отображение сообщения об ошибке
      }
    } catch (error) {
      console.error('Error during login:', error);
    }
  }
}
