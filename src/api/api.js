import { BrowserAPI } from './browser.js';

export class API {
  #browser;

  constructor() {
    this.#browser = new BrowserAPI();
  }

  async login(email, password) {
      return this.#browser.login(email, password)
  }
}
