import { useState } from 'preact/hooks'
import styles from "./signup.module.css"
import { route } from 'preact-router';


export function Signup() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [city, setCity] = useState('');
  const [country, setCountry] = useState('country');
  const [firstname, setFirstname] = useState('');
  const [surname, setSurname] = useState('');
  const [middlename, setMiddlename] = useState('');
  const [organization, setOrganization] = useState('');
  const [profession, setProfession] = useState('profession');
  const [professional, setProfessional] = useState('');
  const [promocode, setPromocode] = useState('');
  const list_profession = [ "General Practitioner", "Ophthalmologist", "Endocrinologist", "Optometrist", "Another Doctor", "Medical Representative", "Healthcare Organizer", "Lecturer", "Student", "Researcher", "Other" ]

  const countries = [ "Abkhazia", "Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", "Antigua and Barbuda", "Antilles","Argentina", "Armenia", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium","Belize", "Benin", "Bermuda", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "British virgin islands", "Brunei", "Bulgaria","Burkina Faso", "Burundi", "Butane", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Chad", "Chile", "China", "Colombia", "Congo, Democratic Republic", "Congo", "Cook Islands", "Costa Rica", "Croatia", "Cuba", "Cyprus", "Czech", "Denmark", "Djibouti", "Dominican Republic", "Ecuador", "Egypt", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Faroe Island", "Fiji", "Finland", "France","French Polynesia", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guatemala", "Guernesey", "Guinea-Bissau", "Guinea", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India","Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Ivory Coast", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Mauritania", "Mauritius","Mexico", "Moldova", "Monaco", "Mongolia", "Montenegro", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nepal", "Netherlands", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "North Korea", "Norway", "Oman", "Pakistan", "Palau", "Panama","Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn Islands", "Poland", "Portugal", "Puerto Rico", "Qatar", "Romania", "Russia", "Rwanda", "Réunion", "Saint Kitts and Nevis", "Saint Lucia", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Salvador", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Korea", "South Ossetia", "Spain", "Sri Lanka", "State of Palestine", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey","Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "USA", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican", "Venezuela", "Vietnam", "Wallis and Futuna","Western Sahara", "Yemen", "Zambia", " Zimbabwe",]

  const loginUp = async () => {
    route("/login", true)
  }

  const handleRegistration = async () => {
    try {
    const response = await fetch(
      'https://functions.yandexcloud.net/d4esr6ie5khkuno72hib',
      { 
        method: "post", 
        headers: { 'Accept': 'application/json', 'Content-Type': 'text/plain' }, 
        body: JSON.stringify({ 
          "op": "register",
          email,
          password,
          "first_name": firstname,
          "last_name": surname,
          "middle_name": middlename,
          "country": country,
          "city": city,
          "organization": organization,
          "profession": profession,
          "promocode": promocode,
          "language": "en",
          "domain": "com",
          "name": "",
          "real_name": "",
        }) 
      }
    );

    if (response.status === 200) {
      route('/login', true);
      // Дополнительные действия после успешной регистрации, например, переход на другой экран
      
    } else {
      console.error('Login failed');
        // Добавить обработку ошибок, например, отображение сообщения об ошибке
    } 
    } catch (error) {
    console.error('Error during login:', error);
    }
  };
  
  return (
    <div className={styles.container}>
      <button type="button" onClick={loginUp} className="exit">Выйти</button>
      <div className={styles.signup}>
      <h1>Регистрация</h1>

      <input type="email" name="email" id="email" placeholder="Email" value={email} onChange={(e) => setEmail(e.target.value)} />
      <input type="password" name="password" id="password" placeholder="Пароль" value={password} onChange={(e) => setPassword(e.target.value)} />
      <input type="text" name="city" id="city" placeholder="Город" required value={city} onChange={(e) => setCity(e.target.value)} />

      <select value={country} onChange={(e) => setCountry(e.target.value)}>
        <option value="country">Выберите страну</option>
        {/* List country  */}
     {countries.map((country) => {
        return (<option value={country}>{country}</option>)
      })
     }
      </select>

      <input type="text" name="firstname" id="firstname" placeholder="Имя" value={firstname} onChange={(e) => setFirstname(e.target.value)} />
      <input type="text" name="surname" id="surname" placeholder="Фамилия" value={surname} onChange={(e) => setSurname(e.target.value)} />
      <input type="text" name="middlename" id="middlename" placeholder="Отчество" value={middlename} onChange={(e) => setMiddlename(e.target.value)} />
      <input type="text" name="organization" id="organization" placeholder="Организация" value={organization} onChange={(e) => setOrganization(e.target.value)} />

      <select value={profession} onChange={(e) => setProfession(e.target.value)}>
        <option value="profession">Выберите профессию</option>
        {/* list profession */}
        {list_profession.map((profession) => {
        return (<option value={profession}>{profession}</option>)
      })
     }

      </select>

      <input type="text" name="professional" id="professional" placeholder="Профессия" value={professional} onChange={(e) => setProfessional(e.target.value)} />
      <input type="text" name="promocode" id="promocode" placeholder="Промокод" value={promocode} onChange={(e) => setPromocode(e.target.value)} />

      <button type="button" onClick={handleRegistration}>Зарегистрироваться</button>
    </div>
    </div>
  );
}
