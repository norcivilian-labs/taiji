import { route } from "preact-router";
import { useState } from "preact/hooks";
import  styles  from "./detection.module.css";

export function Detection() { 

  const menuUp = async ({token}) => {
    route("/", true)
  }

  const [file, setFile] = useState(undefined);
  const [objectURL, setObjectURL] = useState("");

  async function onUpload(f) {
    if (f) {
      const objectURL = URL.createObjectURL(f);
      setObjectURL(objectURL);
      setFile(f);
    }
  }


  return (
    // basic container 
 <div className={styles.container}>
   <button type="button" onClick={menuUp} className="exit">Выйти</button>

   {/* main content */}
    <div className={styles.detection}>
    <p> your photo:</p>
   <img object={file} src={objectURL} />
   <input
          type="file"
          onChange={(e) => onUpload(e.target.files[0])}
        />
    </div>
 </div>
  )
}
