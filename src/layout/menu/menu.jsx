import styles from './menu.module.css'


export function Menu() {

  return (
    <div className={styles.menu}>

      <a className={styles.link} href="/fundus">
        Анализ  фундус фото
        диабетическая ретинопатия
      </a>
      <a className={styles.link} href="/profile">
        Профиль
      </a>
      <a className={styles.link} href="/okt">
        Анализ ОКТ
      </a>
    </div>
  );
}
