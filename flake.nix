{
  inputs = { nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable"; };
  outputs = inputs@{ nixpkgs, ... }:
    let
      eachSystem = systems: f:
        let
          op = attrs: system:
            let
              ret = f system;
              op = attrs: key:
                let
                  appendSystem = key: system: ret: { ${system} = ret.${key}; };
                in attrs // {
                  ${key} = (attrs.${key} or { })
                    // (appendSystem key system ret);
                };
            in builtins.foldl' op attrs (builtins.attrNames ret);
        in builtins.foldl' op { } systems;
      defaultSystems = [ "x86_64-linux" "aarch64-darwin" "aarch64-linux" ];
    in eachSystem defaultSystems (system:
      let
        pkgs = import nixpkgs { inherit system; };
        src = pkgs.nix-gitignore.gitignoreSource [ ".git" ] ./.;
        package = (pkgs.lib.importJSON (src + "/package.json"));
        nodeVersion =
          builtins.elemAt (pkgs.lib.versions.splitVersion pkgs.nodejs.version)
          0;
        library = pkgs.mkYarnPackage rec {
          name = package.name;
          version = package.version;
          src = pkgs.nix-gitignore.gitignoreSource [ ".git" ] ./.;
          buildPhase = ''
            true
          '';
          # fixup of libexec hangs for undiscovered reason
          dontStrip = true;
        };
        webappDrv = buildMode:
          pkgs.mkYarnPackage rec {
            name = package.name;
            version = package.version;
            src = pkgs.nix-gitignore.gitignoreSource [ ".git" ] ./.;
            preConfigure = ''
              substituteInPlace package.json --replace "vite build" "yarn exec vite build"
            '';
            buildPhase = ''
              yarn run build
            '';
            installPhase = "cp -r ./deps/${name}/dist $out";
            distPhase = "true";
          };
        webapp = webappDrv "webapp";
      in rec {
        packages = { inherit webapp; };
        defaultPackage = packages.webapp;
        defaultApp = packages.webapp;
        devShell =
          pkgs.mkShell { nativeBuildInputs = with pkgs; [ nodejs yarn ]; };
      });
}
